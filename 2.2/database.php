<?php
/*
 * Author: Poticous
 * 12 March 2014
 * Version 1.0
 */
require('lib/medoo.php');

class Database{

	function __construct(){
		$this->soal = new medoo([
				'database_type' => 'sqlite',
				'database_file' => 'database/Soal.db'
			]);
		$this->score = new medoo([
				'database_type' => 'sqlite',
				'database_file' => 'database/Score.db'
			]);
	}
	public function kosongkan($db){
		$dbase = ($db == 'soal') ? $this->soal : $this->score;
		$dbase->query("DELETE FROM data");
		$dbase->query("DELETE FROM SQLITE_SEQUENCE WHERE name='data'");
	}
	public function struktur($db){
		$score = 'DROP TABLE IF EXISTS "data"; CREATE TABLE [data] ([id] INTEGER PRIMARY KEY AUTOINCREMENT, [userid] INTEGER DEFAULT(0), [total] INTEGER DEFAULT(0), [benar] INTEGER DEFAULT(0));';
		$soal = 'DROP TABLE IF EXISTS "data"; CREATE TABLE "data" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , "pertanyaan" TEXT NOT NULL , "jawaban" TEXT NOT NULL );';
		return ($db == 'soal') ? $soal : $score;
	}
	public function export($db){
		$db = ($db == 'soal') ? 'soal' : 'score';
		$data = ($db == 'soal') ? $this->soal->select("data", "*") : $this->score->select("data", "*");
		$key = array_keys($data[0]);
		$sql = $this->struktur($db)."\n";
		$sql .= "INSERT INTO data (`".implode("`, `", $key)."`) VALUES \n";
		foreach($data as $dt){
			$val = array_values($dt);
			$sql .= "('".implode("', '", $val)."'),\n";
		}
		$sql = rtrim(trim($sql), ",").";";
		$fname = "database/backup/".$db."-".date("d-m-Y H-i-s")."_SQLite.sql";
		$fp = fopen($fname, "w");
		fwrite($fp, $sql);
		fclose($fp);
	}
}
$db = new Database();

if(isset($argv[1])){
	if($argv[1] == 'export'){
		$db->export($argv[2]);
	}elseif($argv[1] == 'empty'){
		$db->kosongkan('score');
	}
}
