<?php
/*
 * Author: Poticous
 * 09 March 2014
 * Edited 12 March 2014
 * Version 2.2
 *  - Menambahkan config.ini
 *  - Mengubah database dari MySQL ke SQLite
 *  - Menghapus type_soal dari database
 */
require('lib/bot.php');
require('lib/medoo.php');

class Trivia extends PotiBot{
	private $soal;
	private $score;
	private $delay_ganti_jawaban;
	private $bot_userid;
	
	public function __construct(){
		$this->soal = new medoo([
				'database_type' => 'sqlite',
				'database_file' => 'database/Soal.db'
			]);
		$this->score = new medoo([
				'database_type' => 'sqlite',
				'database_file' => 'database/Score.db'
			]);
		$this->config = parse_ini_file("config.ini", true);
		$this->cookies = 'cookies/trivia.txt';
		$this->txt_login = $this->config['trivia']['txt_login'];
		$this->emot_code = array_keys($this->config['smilies']);
		$this->emot_img = array_values($this->config['smilies']);
		$this->delay_ganti_jawaban = (isset($this->config['setting']['delay_ganti_jawaban'])) ? $this->config['setting']['delay_ganti_jawaban'] : 15;
		
		$this->situs = $this->config['setting']['situs'];
		$this->bot_userid = $this->config['trivia']['userid'];
		$login = $this->login($this->config['trivia']['username'], $this->config['trivia']['password']);
		if($login){
			echo "Login berhasil. (delay ".$this->config['setting']['delay_login']." detik)\n";
			sleep($this->config['setting']['delay_login']);
			$this->bot();
		}else{
			echo "Bot gagal login -_-\n";
			exit(0);
		}
	}
	public function getJawaban($text){
		preg_match("/^!jawab (.*)/", strtolower($text), $answer);
		return trim($answer[1]);
	}
	public function saveScore($userid, $score){
		$cek = $this->score->select("data", "*", ["userid"=>$userid]);
		if(count($cek) > 0){
			$update = $this->score->update("data", [
						"total[+]" => $score,
						"benar[+]" => 1,
						], ["userid" => $userid]);
		}else{
			$insert = $this->score->insert("data", [
						"userid" => $userid,
						"total" => $score,
						"benar" => 1
						]);
		}
	}
	public function getScore($userid){
		$cek = $this->score->select("data", "*", ["userid"=>$userid]);
		return $cek[0]['total'];
	}
	public function bot(){
		echo "Mencari last_id...\n";
		$shout = $this->getShout($lastid);
		$lastid = $shout['lastid'];
		echo "Mengumpulkan pertanyaan...\n";
		$lsoal = $this->soal->select("data", "*");
		echo "Pertanyaan sudah dikumpulkan...\n";
		$soal = $lsoal[array_rand($lsoal)];
		$total_jawaban = count(explode("|", $soal['jawaban']));
		$list_jawaban = explode("|", $soal['jawaban']);
		$jawaban = array();
		foreach($list_jawaban as $jwb){
			$jawaban[strtolower($jwb)] = strlen($jwb);
		}
		$s_soal = (preg_match("/\|/", $soal['jawaban'])) ? "Soal: ".$soal['pertanyaan']." (Ada ".$total_jawaban." jawaban, total ".strlen(str_replace("|", "", $soal['jawaban']))." point)" : "Soal: ".$soal['pertanyaan'];
		$this->shout($s_soal);
		echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
		$counter = time();
		$score = strlen($soal['jawaban']);
		while(1){
			$shout = $this->getShout($lastid);
			foreach($shout['data'] as $data){
				if($data['userid'] != $this->bot_userid){
					//echo $data['shoutid']." > ".$data['username']." (".$data['date'].") ".$data['text_emot']." => ".$this->getJawaban($data['text_emot'])."\n";
					if(array_key_exists($this->getJawaban($data['text_emot']), $jawaban)){
						$this->saveScore($data['userid'], $jawaban[$this->getJawaban($data['text_emot'])]);
						$this->shout("Selamat ".$data['username']." jawaban anda benar (y) anda mendapatkan point [b]".$jawaban[$this->getJawaban($data['text_emot'])]."[/b].");
						echo "Selamat ".$data['username']." jawaban anda benar (y) anda mendapatkan point ".$jawaban[$this->getJawaban($data['text_emot'])].".\n";
						unset($jawaban[$this->getJawaban($data['text_emot'])]);
						if(empty($jawaban)){
							sleep($this->delay_ganti_jawaban);
							$soal = $lsoal[array_rand($lsoal)];
							echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
							$total_jawaban = count(explode("|", $soal['jawaban']));
							$list_jawaban = explode("|", $soal['jawaban']);
							$jawaban = array();
							foreach($list_jawaban as $jwb){
								$jawaban[strtolower($jwb)] = strlen($jwb);
							}
							$s_soal = (preg_match("/\|/", $soal['jawaban'])) ? "Soal: ".$soal['pertanyaan']." (Ada ".$total_jawaban." jawaban, total ".strlen(str_replace("|", "", $soal['jawaban']))." point)" : "Soal: ".$soal['pertanyaan'];
							$this->shout($s_soal);
							$counter = time();
						}
					}
				}
			}
			$lastid = $shout['lastid'];
			if((time() - $counter) == $this->config['setting']['waktu_beritahu']){
				foreach($jawaban as $jwb_key => $jwb_jwb){
					$jawaban[$jwb_key] = round($jwb_jwb/2);
				}
				$this->shout("Waktu tinggal ".($this->config['setting']['waktu_habis']-$this->config['setting']['waktu_beritahu'])." detik.");
				echo "Waktu tinggal ".($this->config['setting']['waktu_habis']-$this->config['setting']['waktu_beritahu'])." detik.\n";
				sleep(1);
			}elseif((time() - $counter) == $this->config['setting']['waktu_habis']){
				$this->shout("Waktu habis.");
				echo "Waktu habis.\n";
				$soal = $lsoal[array_rand($lsoal)];
				sleep($this->delay_ganti_jawaban);
				echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
				$total_jawaban = count(explode("|", $soal['jawaban']));
				$list_jawaban = explode("|", $soal['jawaban']);
				$jawaban = array();
				foreach($list_jawaban as $jwb){
					$jawaban[strtolower($jwb)] = strlen($jwb);
				}
				$s_soal = (preg_match("/\|/", $soal['jawaban'])) ? "Soal: ".$soal['pertanyaan']." (Ada ".$total_jawaban." jawaban, total ".strlen(str_replace("|", "", $soal['jawaban']))." point)" : "Soal: ".$soal['pertanyaan'];
				$this->shout($s_soal);
				$counter = time();
			}
		}
	}
}
$bot = new Trivia();