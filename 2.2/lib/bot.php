<?php
/*
 * Author: Poticous
 * 08 March 2014
 * Version 2.2
 *  - Fixing Smilies
 */
class PotiBot{
    private $user_agent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)";
    private $image_string = "JmReP";
	private $image_hash = "d9191ec5c59dd149d95c48f9c5a66b14";
	public $txt_login;
	public $situs;
	public $cookies;
	public $emot_img;
	public $emot_code;
	
    public function curl($url,$data="") {
        $ch = curl_init($url);
        if ($data != "") {
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookies); 
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookies);
        $source = curl_exec($ch);
        $this->status = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        $this->error = curl_error($ch);
        curl_close($ch);
        return $source;
    }
	public function config($file){
		return parse_ini_file($file, true);
	}
	public function save($file, $txt){
		$fp = fopen($file, 'w');
		fwrite($fp, $txt);
		fclose($fp);
	}
	public function stringBetween($start, $end, $var){
		return preg_match('{'.preg_quote($start).'(.*?)'.preg_quote($end).'}s',$var,$m) ? $m[1] : '';
	}
    public function shout($chat){
        $data_chat = array("shout_data"=>$chat);
        $exec = $this->curl($this->situs."/xmlhttp.php?action=add_shout",$data_chat);
        return $exec;
    }
	public function getLinkScr($str){
		return preg_replace('/<a .*?(?=href)href=\"([^\"]+)\" .*?<\/a>/si', "$1", $str);
	}
	public function getEmot($str){
		return str_replace($this->emot_img, $this->emot_code, preg_replace('/<img .*?(?=src)src=\"([^\"]+)\" .*?>/si', "$1", $str));
	}
    public function getkey($index=""){
		preg_match("/var my_post_key = \"(.*)\"/",$index,$key);
        return $key[1];
    }
    public function login($user, $pass){
        $data = array(
            "username" => $user,
            "password" => $pass,
            "action" => "do_login",
            "remember" => "yes",
			"imagestring"=>$this->image_string,
			"imagehash"=>$this->image_hash
        );
        $exec = $this->curl($this->situs."/member.php?action=login",$data);
        if(eregi("success",$exec)){
			if(!empty($this->txt_login)){
				$this->shout($this->txt_login);
			}
            return true;
        }else{
            return false;
        }
    }
    public function reputasi($komen,$uid){
        $url_rep = $this->situs."/reputation.php";
        $data_rep = array(
            "my_post_key"=>$this->getkey(),
            "action"=>"do_add",
            "uid"=>$uid,
            "pid"=>"0",
            "reputation"=>"1",
            "comments"=>$komen
        );
        $exec = $this->curl($url_rep,$data_rep);
        return $exec;
    }
	public function getShout($lastid=''){
		$url = $this->situs."/xmlhttp.php?action=show_shouts&last_id=".$lastid;
		$result = array();
		$res = $this->curl($url);
		$part = explode('^--^', $res);
		$result['lastid'] = $part[0];
		$result['count'] = $part[1];
		$result['data'] = array();
		if($part[1] > 0){
			$data = explode('<br />', $part[2]);
			foreach($data as $dt){
				$d = trim($dt);
				if(!empty($d)){
					$shout_id = (int) $this->stringBetween('promptReason(', ');', $d);
					$user_id = (int) $this->stringBetween('pvtAdd(', ');', $d);
					$user_name = trim(strip_tags($this->stringBetween('); return false;" >', ' - ', $d)));
					$date = trim($this->stringBetween(' - ', ' -- ', $d));
					$text = trim(strip_tags($this->getLinkScr($this->stringBetween(' -- ', '#~~~~#Akhir', $d.'#~~~~#Akhir'))));
					$text_emot = trim(strip_tags($this->getLinkScr($this->getEmot($this->stringBetween(' -- ', '#~~~~#Akhir', $d.'#~~~~#Akhir')))));
					array_push($result['data'], array('shoutid'=>$shout_id, 'userid'=>$user_id, 'username'=>$user_name, 'date'=>$date, 'text'=>$text, 'text_emot'=>$text_emot));
				}
			}
		}
		sort($result['data']);
		return $result;
	}
}