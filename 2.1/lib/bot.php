<?php
/*
 * Author: Poticous
 * 08 March 2014
 */
class PotiBot{
    private $user_agent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)";
    private $image_string = "JmReP"; // jika gagal login, ganti dengan captcha baru
	private $image_hash = "d9191ec5c59dd149d95c48f9c5a66b14"; // jika gagal login, ganti dengan captcha baru
	public $txt_login;
	public $situs;
	public $cookies;
	
    public function curl($url,$data="") {
        $ch = curl_init($url);
        if ($data != "") {
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookies); 
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookies);
        $source = curl_exec($ch);
        $this->status = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        $this->error = curl_error($ch);
        curl_close($ch);
        return $source;
    }
    public function login($user, $pass){
        $data = array(
            "username" => $user,
            "password" => $pass,
            "action" => "do_login",
            "remember" => "yes",
			"imagestring"=>$this->image_string,
			"imagehash"=>$this->image_hash
        );
        $exec = $this->curl($this->situs."/member.php?action=login",$data);
        if(eregi("success",$exec)){
			if(!empty($this->txt_login)){
				$this->shout($this->txt_login);
			}
            return true;
        }else{
            return false;
        }
    }
	public function save($file, $txt){
		$fp = fopen($file, 'w');
		fwrite($fp, $txt);
		fclose($fp);
	}
	public function stringBetween($start, $end, $var){
		return preg_match('{'.preg_quote($start).'(.*?)'.preg_quote($end).'}s',$var,$m) ? $m[1] : '';
	}
	public function getLinkScr($str){
		return preg_replace('/<a .*?(?=href)href=\"([^\"]+)\" .*?<\/a>/si', "$1", $str);
	}
	public function getEmot($str){
		$img = array('images/smilies/ampun_dc.gif','images/smilies/ketawa_dc.gif','images/smilies/meluncur_dc.gif','images/smilies/sip_dc.gif','images/smilies/santapbubur_dc.gif','images/smilies/bangga.gif','images/smilies/dead.gif','images/smilies/maling.gif','images/smilies/mewek.gif','images/smilies/semanget.gif','images/smilies/ngakak.gif','images/smilies/pasrah.gif','images/smilies/ngambek.gif','images/smilies/wawa.gif','images/smilies/hore.gif','images/smilies/suram.gif','images/smilies/belajar.gif','images/smilies/prustasi.gif','images/smilies/pinter.gif','images/smilies/gg.gif','images/smilies/top.gif','images/smilies/sabar.gif','images/smilies/tidur.gif','images/smilies/muntah.gif','images/smilies/stress.gif','images/smilies/please.gif','images/smilies/angel1.gif','images/smilies/angel2.gif','images/smilies/benjol.gif','images/smilies/mimisan.gif','images/smilies/bye.gif','images/smilies/nangis.gif','images/smilies/cuteboy.gif','images/smilies/cutegirl.gif','images/smilies/galau.gif','images/smilies/hai.gif','images/smilies/terharu.gif','images/smilies/kalem.gif','images/smilies/relax.gif','images/smilies/detektif.gif','images/smilies/mandi.gif','images/smilies/peringatan.gif','images/smilies/kartumerah.gif','images/smilies/ngupil.gif','images/smilies/peace.gif','images/smilies/camb.gif','images/smilies/capedeh.gif','images/smilies/sarjana.gif','images/smilies/terkejut.gif','images/smilies/tonjok.gif','images/smilies/tersipu.gif','images/smilies/hah.gif','images/smilies/heh.gif','images/smilies/siul.gif','images/smilies/wew.gif','images/smilies/wow.gif','images/smilies/ngantuk.gif','images/smilies/ngopi.gif','images/smilies/hbd.gif','images/smilies/cerdas.gif','images/smilies/cium.gif','images/smilies/bodo.gif','images/smilies/ngetrek.gif','images/smilies/cheer.gif','images/smilies/ngemis.gif','images/smilies/mohon.gif','images/smilies/lega.gif','images/smilies/ngamuk.gif','images/smilies/pusing.gif','images/smilies/chaer.gif','images/smilies/tagih.gif','images/smilies/bingung.gif','images/smilies/berdoa.gif','images/smilies/takut.gif','images/smilies/insomnia.gif','images/smilies/dingin.gif','images/smilies/tolong.gif','images/smilies/batuk.gif','images/smilies/boong.gif','images/smilies/hipnotis.gif','images/smilies/genit.gif','images/smilies/panas.gif','images/smilies/ngumpet.gif','images/smilies/gerah.gif','images/smilies/siapinpisau.gif','images/smilies/gengster.gif','images/smilies/tepokjidat.gif','images/smilies/shock.gif','images/smilies/spa.gif','images/smilies/panik.gif','images/smilies/intelijen.gif','images/smilies/tampan.gif','images/smilies/ngasep.gif','images/smilies/bacit.gif','images/smilies/curiga.gif','images/smilies/kenapa.gif','images/smilies/bola.gif','images/smilies/rocker.gif','images/smilies/entahlah.gif','images/smilies/skripsi.gif','images/smilies/dugem.gif','images/smilies/kesel.gif','images/smilies/penjahat.gif','images/smilies/ribet.gif','images/smilies/senam_pagi4.gif','images/smilies/kick.gif','images/smilies/temenan.gif','images/smilies/adukepala.gif','images/smilies/sehati.gif','images/smilies/ngedije.gif','images/smilies/goyangkiri.gif','images/smilies/goyangkanan.gif','images/smilies/puterkiri.gif','images/smilies/puterkanan.gif','images/smilies/goyang.gif','images/smilies/goyangslow.gif','images/smilies/dance.gif','images/smilies/makan.gif','images/smilies/iseng.gif','images/smilies/silahkan.gif','images/smilies/lempar.gif','images/smilies/nothing.gif','images/smilies/supermen.gif','images/smilies/yes.gif','images/smilies/lemes.gif','images/smilies/nyerah.gif','images/smilies/phew.gif','images/smilies/moonwalk.gif','images/smilies/ngenes.gif','images/smilies/jitak.gif','images/smilies/santai.gif','images/smilies/asik.gif','images/smilies/garing.gif','images/smilies/mupeng.gif','images/smilies/scary movie character.gif','images/smilies/bomb.gif','images/smilies/melet.gif','images/smilies/angry soldier.gif','images/smilies/army 2.gif','images/smilies/exclamation.gif','images/smilies/shutup.gif','images/smilies/bed.gif','images/smilies/laser shooters.gif','images/smilies/marah.gif','images/smilies/love.gif','images/smilies/emoticon lol.gif','images/smilies/at.gif','images/smilies/hammer.gif','images/smilies/metal.gif','images/smilies/kaget.gif','images/smilies/shoot in wall.gif','images/smilies/matabelo.gif','images/smilies/ketawa.gif','images/smilies/fuck.gif','images/smilies/birthday dude.gif','images/smilies/q11.gif','images/smilies/005.gif','images/smilies/1.gif','images/smilies/bolakbalik.gif','images/smilies/ngacir.gif','images/smilies/x_x.png','images/smilies/ppb (12).gif','images/smilies/ppb (18).gif','images/smilies/ppb (36).gif','images/smilies/breakpc2.gif','images/smilies/nemoflow.png','images/smilies/evil.png','images/smilies/smiley_beer.gif','images/smilies/smoke.gif','images/smilies/mawar.gif','images/smilies/ahhhhhhh.GIF','images/smilies/sponge bob.gif','images/smilies/panda.gif','images/smilies/squirrel run.gif','/favicon.ico','images/ceklist.jpg','images/smilies/karung.gif','images/smilies/sungkem.gif','images/smilies/iwakpeyek.gif');
		$code = array(':ampun',':ketawa',':meluncur',':sip',':santapbubur',':bangga',':dead',':maling',':mewek',':smangat',':ngakak',':pasrah',':ngambek',':wawa',':hore',':suram',':belajar',':prustasi',':pinter',':hmm',':mantap',':sabar',':tidur',':muntah',':stress',':please',':angel1',':angel2',':benjol',':mimisan',':bye',':nangis',':cuteboy',':cutegirl',':galau',':hai',':terharu',':kalem',':relax',':detektif',':mandi',':kartukuning',':kartumerah',':ngupil',':peace',':cambuk',':capedeh',':sarjana',':terkejut',':tonjok',':tersipu',':hah',':heh',':siul',':wew',':wow',':ngantuk',':ngopi',':hbd',':cerdas',':cium',':bodo',':ngetrek',':cheer',':ngemis',':mohon',':lega',':ngamuk',':pusing',':chaer',':tagih',':bingung',':berdoa',':takut',':insomnia',':dingin',':tolong',':batuk',':boong',':hipnotis',':genit',':panas',':ngumpet',':gerah',':siapinpisau',':gengster',':tepokjidat',':shock',':spa',':panik',':intelijen',':tampan',':ngasep',':bacit',':curiga',':why',':bola',':rocker',':entahlah',':skripsi',':dugem',':kesel',':seneng',':ribet',':senampagi',':tendang',':temenan',':adukepala',':sehati',':ngedije',':goyangkiri',':goyangkanan',':puterkiri',':puterkanan',':goyang',':goyangslow',':dance',':makan',':iseng',':silahkan',':lempar',':nothing',':supermen',':yes',':lemes',':nyerah',':phew',':mw',':ngenes',':jitak',':santai',':asik',':garing',':mupeng',':cihuy',':bomb',':P',':angry',':army',':exclamation',':shutup',':bobo',':laser',':mad',':love',':lol',':at',':hammer',':metal',':kaget',':shoot',':matabelo',':D',':fuck',':birthday',':nohope',':piss',':malu',':monman',':ngacir',':x_x',':pbb',':shut up',':dordor',':breakpc',':sombong',':evil',':smiley_beer',':smoke',':mawar',':ahhhh',':spongebob',':panda',':squirrel',':logodc',':ceklist',':karung',':sungkem',':iwakpeyek');
		return str_replace($img, $code, preg_replace('/<img .*?(?=src)src=\"([^\"]+)\" .*?>/si', "$1", $str));
	}
    public function getkey($index=""){
		preg_match("/var my_post_key = \"(.*)\"/",$index,$key);
        return $key[1];
    }
    public function reputasi($komen,$uid){
        $url_rep = $this->situs."/reputation.php";
        $data_rep = array(
            "my_post_key"=>$this->getkey(),
            "action"=>"do_add",
            "uid"=>$uid,
            "pid"=>"0",
            "reputation"=>"1",
            "comments"=>$komen
        );
        $exec = $this->curl($url_rep,$data_rep);
        return $exec;
    }
    public function shout($chat){
        $data_chat = array("shout_data"=>$chat);
        $exec = $this->curl($this->situs."/xmlhttp.php?action=add_shout",$data_chat);
        return $exec;
    }
	public function getShout($lastid=''){
		$url = $this->situs."/xmlhttp.php?action=show_shouts&last_id=".$lastid;
		$result = array();
		$res = $this->curl($url);
		$part = explode('^--^', $res);
		$result['lastid'] = $part[0];
		$result['count'] = $part[1];
		$result['data'] = array();
		if($part[1] > 0){
			$data = explode('<br />', $part[2]);
			foreach($data as $dt){
				$d = trim($dt);
				if(!empty($d)){
					$shout_id = (int) $this->stringBetween('promptReason(', ');', $d);
					$user_id = (int) $this->stringBetween('pvtAdd(', ');', $d);
					$user_name = trim(strip_tags($this->stringBetween('); return false;" >', ' - ', $d)));
					$date = trim($this->stringBetween(' - ', ' -- ', $d));
					$text = trim(strip_tags($this->getLinkScr($this->stringBetween(' -- ', '#~~~~#Akhir', $d.'#~~~~#Akhir'))));
					$text_emot = trim(strip_tags($this->getLinkScr($this->getEmot($this->stringBetween(' -- ', '#~~~~#Akhir', $d.'#~~~~#Akhir')))));
					array_push($result['data'], array('shoutid'=>$shout_id, 'userid'=>$user_id, 'username'=>$user_name, 'date'=>$date, 'text'=>$text, 'text_emot'=>$text_emot));
				}
			}
		}
		sort($result['data']);
		return $result;
	}
}