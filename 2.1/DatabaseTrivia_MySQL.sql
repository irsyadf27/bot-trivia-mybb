--
-- Database: `trivia`
--

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE IF NOT EXISTS `score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `benar` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;


-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE IF NOT EXISTS `soal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` text COLLATE latin1_general_ci NOT NULL,
  `jawaban` text COLLATE latin1_general_ci NOT NULL,
  `type_soal` enum('biasa','banyak') COLLATE latin1_general_ci NOT NULL DEFAULT 'biasa',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `jawaban` (`jawaban`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=82 ;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id`, `pertanyaan`, `jawaban`, `type_soal`) VALUES
(1, 'Huruf A adalah huruf ke ?', '1', 'biasa'),
(2, 'Huruf B adalah huruf ke ?', '2', 'biasa'),
(3, 'Huruf C adalah huruf ke ?', '3', 'biasa'),
(4, 'Huruf D adalah huruf ke ?', '4', 'biasa'),
(5, 'Huruf E adalah huruf ke ?', '5', 'biasa'),
(6, 'Huruf F adalah huruf ke ?', '6', 'biasa'),
(7, 'Huruf G adalah huruf ke ?', '7', 'biasa'),
(8, 'Huruf H adalah huruf ke ?', '8', 'biasa'),
(9, 'Huruf I adalah huruf ke ?', '9', 'biasa'),
(10, 'Huruf J adalah huruf ke ?', '10', 'biasa'),
(11, 'Huruf K adalah huruf ke ?', '11', 'biasa'),
(12, 'Huruf L adalah huruf ke ?', '12', 'biasa'),
(13, 'Huruf M adalah huruf ke ?', '13', 'biasa'),
(14, 'Huruf N adalah huruf ke ?', '14', 'biasa'),
(15, 'Huruf O adalah huruf ke ?', '15', 'biasa'),
(16, 'Huruf P adalah huruf ke ?', '16', 'biasa'),
(17, 'Huruf Q adalah huruf ke ?', '17', 'biasa'),
(18, 'Huruf R adalah huruf ke ?', '18', 'biasa'),
(19, 'Huruf S adalah huruf ke ?', '19', 'biasa'),
(20, 'Huruf T adalah huruf ke ?', '20', 'biasa'),
(21, 'Huruf U adalah huruf ke ?', '21', 'biasa'),
(22, 'Huruf V adalah huruf ke ?', '22', 'biasa'),
(23, 'Huruf W adalah huruf ke ?', '23', 'biasa'),
(24, 'Huruf X adalah huruf ke ?', '24', 'biasa'),
(25, 'Huruf Y adalah huruf ke ?', '25', 'biasa'),
(26, 'Huruf Z adalah huruf ke ?', '26', 'biasa'),
(27, 'Ibukota dari Provinsi Aceh ?', 'Banda Aceh', 'biasa'),
(28, 'Ibukota dari Provinsi Sumatera Utara ?', 'Medan', 'biasa'),
(29, 'Ibukota dari Provinsi Sumatera Barat ?', 'Padang', 'biasa'),
(30, 'Ibukota dari Provinsi Riau ?', 'Pekan Baru', 'biasa'),
(31, 'Ibukota dari Provinsi Kepulauan Riau ?', 'Tanjung Pinang', 'biasa'),
(32, 'Ibukota dari Provinsi Jambi ?', 'Jambi', 'biasa'),
(33, 'Ibukota dari Provinsi Sumatera Selatan ?', 'Palembang', 'biasa'),
(34, 'Ibukota dari Provinsi Bangka Belitung ?', 'Pangkal Pinang', 'biasa'),
(35, 'Ibukota dari Provinsi Bengkulu ?', 'Bengkulu', 'biasa'),
(36, 'Ibukota dari Provinsi Lampung ?', 'Bandar Lampung', 'biasa'),
(37, 'Ibukota dari Provinsi DKI Jakarta ?', 'Jakarta', 'biasa'),
(38, 'Ibukota dari Provinsi Jawa Barat ?', 'Bandung', 'biasa'),
(39, 'Ibukota dari Provinsi Banten ?', 'Serang', 'biasa'),
(40, 'Ibukota dari Provinsi Jawa Tengah ?', 'Semarang', 'biasa'),
(41, 'Ibukota dari Provinsi Daerah Istimewa Yogyakarta ?', 'Yogyakarta', 'biasa'),
(42, 'Ibukota dari Provinsi Jawa Timur ?', 'Surabaya', 'biasa'),
(43, 'Ibukota dari Provinsi Bali ?', 'Denpasar', 'biasa'),
(44, 'Ibukota dari Provinsi Nusa Tenggara Barat ?', 'Mataram', 'biasa'),
(45, 'Ibukota dari Provinsi Nusa Tenggara Timur ?', 'Kupang', 'biasa'),
(46, 'Ibukota dari Provinsi Kalimantan Utara ?', 'Tanjung Selor', 'biasa'),
(47, 'Ibukota dari Provinsi Kalimantan Barat ?', 'Pontianak', 'biasa'),
(48, 'Ibukota dari Provinsi Kalimantan Tengah ?', 'Palangkaraya', 'biasa'),
(49, 'Ibukota dari Provinsi Kalimantan Selatan ?', 'Banjarmasin', 'biasa'),
(50, 'Ibukota dari Provinsi Kalimantan Timur ?', 'Samarinda', 'biasa'),
(51, 'Ibukota dari Provinsi Sulawesi Utara ?', 'Manado', 'biasa'),
(52, 'Ibukota dari Provinsi Sulawesi Barat ?', 'Kota Mamuju', 'biasa'),
(53, 'Ibukota dari Provinsi Sulawesi Tengah ?', 'Palu', 'biasa'),
(54, 'Ibukota dari Provinsi Sulawesi Tenggara ?', 'Kendari', 'biasa'),
(55, 'Ibukota dari Provinsi Sulawesi Selatan ?', 'Makassar', 'biasa'),
(56, 'Ibukota dari Provinsi Gorontalo ?', 'Gorontalo', 'biasa'),
(57, 'Ibukota dari Provinsi Maluku ?', 'Ambon', 'biasa'),
(58, 'Ibukota dari Provinsi Maluku Utara ?', 'Ternate', 'biasa'),
(59, 'Ibukota dari Provinsi Papua Barat ?', 'Kota Manokwari', 'biasa'),
(60, 'Ibukota dari Provinsi Papua ?', 'Jayapura', 'biasa'),
(61, 'Orang terkeren di devilzc0de ?', 'poticous', 'biasa'),
(62, 'Mata uang negara berawalan huruf A', 'Afgani', 'banyak'),
(63, 'Mata uang negara berawalan huruf B', 'Balboa|Bath|Birr|Bolivar|Boliviamus', 'banyak'),
(64, 'Mata uang negara berawalan huruf C', 'Cedi|Courde|Cruzeiro', 'banyak'),
(65, 'Mata uang negara berawalan huruf D', 'Deutsche Mark|Dinar|Dirham|Dollar|Dong|Drachma|Dram', 'banyak'),
(66, 'Mata uang negara berawalan huruf E', 'Escudo', 'banyak'),
(67, 'Mata uang negara berawalan huruf F', 'Forint|Franc|Frenc', 'banyak'),
(68, 'Mata uang negara berawalan huruf G', 'Guarani|Guilder|Gulden', 'banyak'),
(69, 'Mata uang negara berawalan huruf I', 'Imani', 'banyak'),
(70, 'Mata uang negara berawalan huruf K', 'Kina|Kolon|Kordoba|Koruna|Krona|Krone|Kroon|Kwacha|Kwanza|Kyat', 'banyak'),
(71, 'Mata uang negara berawalan huruf L', 'Lek|Lempira|Leu|Lev|Lira', 'banyak'),
(72, 'Mata uang negara berawalan huruf M', 'Manat|Markka', 'banyak'),
(73, 'Mata uang negara berawalan huruf N', 'Naira|New Kip', 'banyak'),
(74, 'Mata uang negara berawalan huruf P', 'Peseta|Peso|Pound|Pound Sterling', 'banyak'),
(75, 'Mata uang negara berawalan huruf Q', 'Queizal', 'banyak'),
(76, 'Mata uang negara berawalan huruf R', 'Rand|Real|Rial|Riel|Riyal|Rouble|Rubel|Rupee|Rupes|Rupiah|Ringgit', 'banyak'),
(77, 'Mata uang negara berawalan huruf S', 'Shilling|Sole|Som|Sucrve', 'banyak'),
(78, 'Mata uang negara berawalan huruf T', 'Taka|Tenge|Tugrik', 'banyak'),
(79, 'Mata uang negara berawalan huruf W', 'Won', 'banyak'),
(80, 'Mata uang negara berawalan huruf Y', 'Yen|Yuan', 'banyak'),
(81, 'Mata uang negara berawalan huruf Z', 'Zaire|zioty', 'banyak');
