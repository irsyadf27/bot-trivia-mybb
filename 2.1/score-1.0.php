<?php
/*
 * Author: Poticous
 * 09 March 2014
 */
require('lib/bot.php');
require('lib/medoo.php');

class Score extends PotiBot{
	private $db;
	public $bot_userid;
	
	public function __construct(){
		$this->cookies = 'cookies_score.txt';
		$this->txt_login = "/me Bot hadir kembali :metal";
		/* Untuk menggunakan database MySQL
		$this->db = new medoo([
					'database_type' => 'mysql',
					'database_name' => 'trivia',
					'server' => 'localhost',
					'username' => 'root',
					'password' => ''
				]);
		*/
		/* Untuk menggunakan database SQLite */
		$this->db = new medoo([
				'database_type' => 'sqlite',
				'database_file' => 'DatabaseTrivia.db'
			]);
	}
	public function getUserid($text){
		if(preg_match('/^!score/', $text)){
			preg_match("/^!score ([0-9]+)/", strtolower($text), $score);
			if(!empty($score[1])){
				return (preg_match('/[^0-9]/', trim($score[1]))) ? false : trim($score[1]);
			}else{
				return 'userid_saya';
			}
		}
	}
	public function getScore($userid){
		//$cek = $this->db->query("SELECT userid, total, benar, FIND_IN_SET(total, (SELECT GROUP_CONCAT( total ORDER BY total DESC ) FROM score)) AS rank FROM score WHERE userid= '".$userid."'")->fetch();
		$cek = $this->db->query("SELECT  score1.*, (SELECT COUNT(*)+1 FROM score AS score2 WHERE score2.total > score1.total) AS rank FROM score AS score1 WHERE score1.userid= '".$userid."'")->fetch();
		return $cek;
	}
	public function bot(){
		echo "Mencari last_id...\n";
		$shout = $this->getShout($lastid);
		$lastid = $shout['lastid'];
		while(1){
			$shout = $this->getShout($lastid);
			foreach($shout['data'] as $data){
				if($data['userid'] != $this->bot_userid){
					//echo $data['shoutid']." > ".$data['username']." (".$data['date'].") ".$data['text_emot']."\n";
					$getUid = $this->getUserid($data['text_emot']);
					if($getUid){
						if($getUid == 'userid_saya'){
							$score = $this->getScore($data['userid']);
						}else{
							$score = $this->getScore($getUid);
						}
						if($score){
							$this->shout("[b]@".$data['username']."[/b]: score userid ".$score['userid']." adalah ".$score['total']." berada di ranking ".$score['rank']." dengan menjawab ".$score['benar']." pertanyaan :metal");
							echo "@".$data['username'].": score userid ".$score['userid']." adalah ".$score['total']." berada di ranking ".$score['rank']." dengan menjawab ".$score['benar']." pertanyaan :metal\n";
						}else{
							$this->shout("[b]@".$data['username']."[/b]: user tersebut tidak ada di database");
							echo "@".$data['username'].": user tersebut tidak ada di database\n";
						}
					}
				}
			}
			$lastid = $shout['lastid'];
		}
	}
}
$bot = new Score();

$bot->situs = "http://127.0.0.1/mybb/";
$bot->bot_userid = 2; //userid bot
$login = $bot->login('USERNAME', 'PASSWORD');
if($login){
	$bot->bot();
}else{
	echo "Bot gagal login -_-\n";
}
