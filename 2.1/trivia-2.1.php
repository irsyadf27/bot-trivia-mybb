<?php
/*
 * Author: Poticous
 * 09 March 2014
 */
require('lib/bot.php');
require('lib/medoo.php');

class Trivia extends PotiBot{
	private $db;
	public $bot_userid;
	
	public function __construct(){
		$this->cookies ='cookies_trivia.txt';
		$this->txt_login = "/me Bot hadir kembali :metal";
		/* Untuk menggunakan database MySQL
		$this->db = new medoo([
					'database_type' => 'mysql',
					'database_name' => 'trivia',
					'server' => 'localhost',
					'username' => 'root',
					'password' => ''
				]);
		*/
		/* Untuk menggunakan database SQLite */
		$this->db = new medoo([
				'database_type' => 'sqlite',
				'database_file' => 'DatabaseTrivia.db'
			]);
	}
	public function getJawaban($text){
		preg_match("/^!jawab (.*)/", strtolower($text), $answer);
		return trim($answer[1]);
	}
	public function saveScore($userid, $score){
		$cek = $this->db->select("score", "*", ["userid"=>$userid]);
		if(count($cek) > 0){
			$update = $this->db->update("score", [
						"total[+]" => $score,
						"benar[+]" => 1,
						], ["userid" => $userid]);
		}else{
			$insert = $this->db->insert("score", [
						"userid" => $userid,
						"total" => $score,
						"benar" => 1
						]);
		}
	}
	public function getScore($userid){
		$cek = $this->db->select("score", "*", ["userid"=>$userid]);
		return $cek[0]['total'];
	}
	public function bot(){
		echo "Mencari last_id...\n";
		$shout = $this->getShout($lastid);
		$lastid = $shout['lastid'];
		echo "Mengumpulkan pertanyaan...\n";
		$lsoal = $this->db->select("soal", "*");
		echo "Pertanyaan sudah dikumpulkan...\n";
		$soal = $lsoal[array_rand($lsoal)];
		$total_jawaban = count(explode("|", $soal['jawaban']));
		$list_jawaban = explode("|", $soal['jawaban']);
		$jawaban = array();
		foreach($list_jawaban as $jwb){
			$jawaban[strtolower($jwb)] = strlen($jwb);
		}
		$s_soal = ($soal['type_soal'] == 'biasa') ? "Soal: ".$soal['pertanyaan'] : "Soal: ".$soal['pertanyaan']." (Ada ".$total_jawaban." jawaban)";
		$this->shout($s_soal);
		echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
		$counter = time();
		$score = strlen($soal['jawaban']);
		while(1){
			$shout = $this->getShout($lastid);
			foreach($shout['data'] as $data){
				if($data['userid'] != $this->bot_userid){
					echo $data['shoutid']." > ".$data['username']." (".$data['date'].") ".$data['text_emot']." => ".$this->getJawaban($data['text_emot'])."\n";
					if(array_key_exists($this->getJawaban($data['text_emot']), $jawaban)){
						$this->saveScore($data['userid'], $jawaban[$this->getJawaban($data['text_emot'])]);
						$this->shout("Selamat ".$data['username']." jawaban anda benar (y) anda mendapatkan score [b]".$jawaban[$this->getJawaban($data['text_emot'])]."[/b].");
						echo "Selamat ".$data['username']." jawaban anda benar (y) anda mendapatkan score ".$jawaban[$this->getJawaban($data['text_emot'])].".\n";
						unset($jawaban[$this->getJawaban($data['text_emot'])]);
						if(empty($jawaban)){
							sleep(30);
							$soal = $lsoal[array_rand($lsoal)];
							echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
							$total_jawaban = count(explode("|", $soal['jawaban']));
							$list_jawaban = explode("|", $soal['jawaban']);
							$jawaban = array();
							foreach($list_jawaban as $jwb){
								$jawaban[strtolower($jwb)] = strlen($jwb);
							}
							$s_soal = ($soal['type_soal'] == 'biasa') ? "Soal: ".$soal['pertanyaan'] : "Soal: ".$soal['pertanyaan']." (Ada ".$total_jawaban." jawaban)";
							$this->shout($s_soal);
							$counter = time();
						}
					}
				}
			}
			$lastid = $shout['lastid'];
			if((time() - $counter) == 60){ // 60 detik
				foreach($jawaban as $jwb_key => $jwb_jwb){
					$jawaban[$jwb_key] = round($jwb_jwb/2);
				}
				$this->shout("Waktu tinggal 60 detik.");
				echo "Waktu tinggal 60 detik.\n";
				sleep(1);
			}elseif((time() - $counter) == 120){ // 120 detik
				$this->shout("Waktu habis.");
				echo "Waktu habis.\n";
				$soal = $lsoal[array_rand($lsoal)];
				sleep(30);
				echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
				$total_jawaban = count(explode("|", $soal['jawaban']));
				$list_jawaban = explode("|", $soal['jawaban']);
				$jawaban = array();
				foreach($list_jawaban as $jwb){
					$jawaban[strtolower($jwb)] = strlen($jwb);
				}
				$s_soal = ($soal['type_soal'] == 'biasa') ? "Soal: ".$soal['pertanyaan'] : "Soal: ".$soal['pertanyaan']." (Ada ".$total_jawaban." jawaban)";
				$this->shout($s_soal);
				$counter = time();
			}
		}
	}
}
$bot = new Trivia();

$bot->situs = "http://127.0.0.1/mybb/";
$bot->bot_userid = 2; //userid bot
$login = $bot->login('USERNAME', 'PASSWORD');
if($login){
	$bot->bot();
}else{
	echo "Bot gagal login -_-\n";
}
