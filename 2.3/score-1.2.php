<?php
/*
 * Author: Poticous
 * 09 March 2014
 * Edited 12 March 2014
 * Version 1.2
 *  - Memisahkan Database Score Dengan Soal
 */
require('lib/bot.php');
require('lib/medoo.php');

class Score extends PotiBot{
	private $db;
	public $bot_userid;
	
	public function __construct(){
		parent::__construct();
		$this->db = new medoo([
				'database_type' => 'sqlite',
				'database_file' => 'database/Score.db'
			]);
		$this->config = parse_ini_file("config.ini", true);
		$this->txt_login = $this->config['score']['txt_login'];
		
		$this->situs = $this->config['setting']['situs'];
		$this->bot_userid = $this->config['score']['userid'];
		$login = $this->login($this->config['score']['username'], $this->config['score']['password']);
		if($login){
			echo "Login berhasil. (delay ".$this->config['setting']['delay_login']." detik)\n";
			sleep($this->config['setting']['delay_login']);
			$this->bot();
		}else{
			echo "Bot gagal login -_-\n";
			exit(0);
		}
	}
	public function getUserid($text){
		if(preg_match('/^!score/', $text)){
			preg_match("/^!score ([0-9]+)/", strtolower($text), $score);
			if(!empty($score[1])){
				return (preg_match('/[^0-9]/', trim($score[1]))) ? false : trim($score[1]);
			}else{
				return 'userid_saya';
			}
		}
	}
	public function getScore($userid){
		//$cek = $this->db->query("SELECT userid, total, benar, FIND_IN_SET(total, (SELECT GROUP_CONCAT( total ORDER BY total DESC ) FROM score)) AS rank FROM score WHERE userid= '".$userid."'")->fetch();
		$cek = $this->db->query("SELECT  score1.*, (SELECT COUNT(*)+1 FROM data AS score2 WHERE score2.total > score1.total) AS rank FROM data AS score1 WHERE score1.userid= '".$userid."'")->fetch();
		return $cek;
	}
	public function bot(){
		echo "Mencari last_id...\n";
		$shout = $this->getShout($lastid);
		$lastid = $shout['lastid'];
		while(1){
			$shout = $this->getShout($lastid);
			foreach($shout['data'] as $data){
				if($data['userid'] != $this->bot_userid){
					//echo $data['shoutid']." > ".$data['username']." (".$data['date'].") ".$data['text_emot']."\n";
					$getUid = $this->getUserid($data['text_emot']);
					if($getUid){
						if($getUid == 'userid_saya'){
							$score = $this->getScore($data['userid']);
						}else{
							$score = $this->getScore($getUid);
						}
						if($score){
							$this->shout("[b]@".$data['username']."[/b]: score userid ".$score['userid']." adalah ".$score['total']." berada di ranking ".$score['rank']." dengan menjawab ".$score['benar']." pertanyaan :metal");
							echo "@".$data['username'].": score userid ".$score['userid']." adalah ".$score['total']." berada di ranking ".$score['rank']." dengan menjawab ".$score['benar']." pertanyaan :metal\n";
						}else{
							$this->shout("[b]@".$data['username']."[/b]: user tersebut tidak ada di database");
							echo "@".$data['username'].": user tersebut tidak ada di database\n";
						}
					}
				}
			}
			$lastid = $shout['lastid'];
		}
	}
}
$bot = new Score();