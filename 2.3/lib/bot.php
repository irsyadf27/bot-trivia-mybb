<?php
/*
 * Author: Poticous
 * 08 March 2014
 * Version 2.3
 *  - Mengganti CURL dengan class snoopy
 *  - Fixing Gagal login
 */
require_once('Snoopy.class.php');
class PotiBot{
	public $txt_login;
	public $situs;
	public $cookies;
	public $emot_img;
	public $emot_code;
	private $snoopy;
	private $post_key;
	
	public function __construct(){
		$this->snoopy = new Snoopy();
		$this->snoopy->agent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)";
		$this->snoopy->rawheaders = array(
			"Accept" => "text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
			"Accept-Language" => "en-gb,en;q=0.5",
			"Accept-Charset" => "ISO-8859-1,utf-8;q=0.7,*;q=0.7",
			"Connection" => "keep-alive"
			);
	}
	public function config($file){
		return parse_ini_file($file, true);
	}
	public function save($file, $txt){
		$fp = fopen($file, 'w');
		fwrite($fp, $txt);
		fclose($fp);
	}
	public function stringBetween($start, $end, $var){
		return preg_match('{'.preg_quote($start).'(.*?)'.preg_quote($end).'}s',$var,$m) ? $m[1] : '';
	}
    public function shout($chat){
        $data_chat = array("shout_data"=>$chat);
        $exec = $this->snoopy->submit($this->situs."/xmlhttp.php?action=add_shout",$data_chat);
        return $exec->results;
    }
	public function getLinkScr($str){
		return preg_replace('/<a .*?(?=href)href=\"([^\"]+)\" .*?<\/a>/si', "$1", $str);
	}
	public function getEmot($str){
		return str_replace($this->emot_img, $this->emot_code, preg_replace('/<img .*?(?=src)src=\"([^\"]+)\" .*?>/si', "$1", $str));
	}
    public function login($user, $pass){
        $data = array(
			"quick_login" => "1",
            "quick_username" => $user,
            "quick_password" => $pass,
            "action" => "do_login",
            "quick_remember" => "yes",
        );
		if($this->snoopy->submit($this->situs."/member.php", $data)){
			if(preg_match('/success/', $this->snoopy->results)){
				foreach($this->snoopy->headers as $header){
					if(preg_match("/Set-Cookie: ([^;]+);/", $header, $out)){
						$data_header = explode("=",$out[1]);
						if(count($data_header)==2){
							$this->snoopy->cookies[$data_header[0]] = $data_header[1];
						}
					}
				}
				$this->snoopy->fetch($this->situs."/index.php");
				$html = $this->snoopy->results;
				if(preg_match("/var\ my_post_key\ =\ \"([^\"]{32})\"/is", $html, $key)) $this->post_key = $key[1];
				if(!empty($this->txt_login)) $this->shout($this->txt_login);
				return true;
			}
			return false;
		}
		return false;
    }
    public function reputasi($komen,$uid){
        $url_rep = $this->situs."/reputation.php";
        $data_rep = array(
            "my_post_key"=>$this->post_key,
            "action"=>"do_add",
            "uid"=>$uid,
            "pid"=>"0",
            "reputation"=>"1",
            "comments"=>$komen
        );
        $exec = $this->snoopy->submit($url_rep,$data_rep);
        return $exec;
    }
	public function getShout($lastid=''){
		$this->snoopy->fetch($this->situs."/xmlhttp.php?action=show_shouts&last_id=".$lastid);
		$result = array();
		$res = $this->snoopy->results;
		$part = explode('^--^', $res);
		$result['lastid'] = $part[0];
		$result['count'] = $part[1];
		$result['data'] = array();
		if($part[1] > 0){
			$data = explode('<br />', $part[2]);
			foreach($data as $dt){
				$d = trim($dt);
				if(!empty($d)){
					$shout_id = (int) $this->stringBetween('promptReason(', ');', $d);
					$user_id = (int) $this->stringBetween('pvtAdd(', ');', $d);
					$user_name = trim(strip_tags($this->stringBetween('); return false;" >', ' - ', $d)));
					$date = trim($this->stringBetween(' - ', ' -- ', $d));
					$text = trim(strip_tags($this->getLinkScr($this->stringBetween(' -- ', '#~~~~#Akhir', $d.'#~~~~#Akhir'))));
					$text_emot = trim(strip_tags($this->getLinkScr($this->getEmot($this->stringBetween(' -- ', '#~~~~#Akhir', $d.'#~~~~#Akhir')))));
					array_push($result['data'], array('shoutid'=>$shout_id, 'userid'=>$user_id, 'username'=>$user_name, 'date'=>$date, 'text'=>$text, 'text_emot'=>$text_emot));
				}
			}
		}
		sort($result['data']);
		return $result;
	}
}