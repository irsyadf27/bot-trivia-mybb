echo off
Title Bot Trivia Mybb (myshoutbox) By Poticous
Mode 80,20
Color 0B
:menu
Cls
echo ooooooooo.                 .    o8o                                           
echo `888   `Y88.             .o8    `"'                                           
echo  888   .d88'  .ooooo.  .o888oo oooo   .ooooo.   .ooooo.  oooo  oooo   .oooo.o 
echo  888ooo88P'  d88' `88b   888   `888  d88' `"Y8 d88' `88b `888  `888  d88(  "8 
echo  888         888   888   888    888  888       888   888  888   888  `"Y88b.  
echo  888         888   888   888 .  888  888   .o8 888   888  888   888  o.  )88b 
echo o888o        `Y8bod8P'   "888" o888o `Y8bod8P' `Y8bod8P'  `V88V"V8P' 8""888P'  
echo.
echo                                                   Bot Trivia Mybb (myshoutbox)
echo.
echo [1] Jalankan Bot Trivia
echo [2] Jalankan Bot Score
echo [3] Export Database Soal
echo [4] Export Database Score
echo [5] Reset Score
echo [6] Exit
echo.
set /p "pil=Pilihan: "
if not defined pil goto menu
if %pil%==1 goto run_trivia
if %pil%==2 goto run_score
if %pil%==3 goto export_soal
if %pil%==4 goto export_score
if %pil%==5 goto reset_score
if %pil%==6 goto exit
goto menu

:run_trivia
Title Running Bot Trivia...
cls
echo Menjalankan Bot Trivia
C:\xampp\php\php.exe -C trivia-2.3.php
goto menu

:run_score
Title Running Bot Score...
cls
echo Menjalankan Bot Score
C:\xampp\php\php.exe -C score-1.2.php
goto menu

:export_soal
Title Export Database Soal...
cls
echo Export Database Soal
C:\xampp\php\php.exe -C database.php export soal
goto menu

:export_score
Title Export Database Score...
cls
echo Export Database Score
C:\xampp\php\php.exe -C database.php export score
goto menu

:reset_score
Title Empty Database Score...
cls
echo Empty Database Score
C:\xampp\php\php.exe -C database.php empty score
goto menu

:exit
exit