<?php
require('lib/bot.php');
require('lib/medoo.php');

class Trivia extends PotiBot{
	private $db;
	public $bot_userid;
	
	public function __construct(){
		$this->db = new medoo([
					'database_type' => 'mysql',
					'database_name' => 'trivia',
					'server' => 'localhost',
					'username' => 'root',
					'password' => ''
				]);
	}
	public function getJawaban($text){
		preg_match("/^!jawab (.*)/", strtolower($text), $answer);
		return trim($answer[1]);
	}
	public function saveScore($userid, $score){
		$cek = $this->db->select("score", "*", ["userid"=>$userid]);
		if(count($cek) > 0){
			$update = $this->db->update("score", [
						"total[+]" => $score,
						"benar[+]" => 1,
						], ["userid" => $userid]);
		}else{
			$insert = $this->db->insert("score", [
						"userid" => $userid,
						"total" => $score,
						"benar" => 1
						]);
		}
	}
	public function getScore($userid){
		$cek = $this->db->select("score", "*", ["userid"=>$userid]);
		return $cek[0]['total'];
	}
	public function bot(){
		echo "Mencari last_id...\n";
		$shout = $this->getShout($lastid);
		$lastid = $shout['lastid'];
		echo "Mengumpulkan pertanyaan...\n";
		$lsoal = $this->db->select("soal", "*", ['type_soal'=>'biasa']);
		echo "Pertanyaan sudah dikumpulkan...\n";
		$soal = $lsoal[array_rand($lsoal)];
		$this->shout("Soal: ".$soal['pertanyaan']);
		echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
		$counter = time();
		$score = 10;
		while(1){
			$shout = $this->getShout($lastid);
			foreach($shout['data'] as $data){
				if($data['userid'] != $this->bot_userid){
					echo $data['shoutid']." > ".$data['username']." (".$data['date'].") ".$data['text_emot']." => ".$this->getJawaban($data['text_emot'])."\n";
					if($this->getJawaban($data['text_emot']) == strtolower($soal['jawaban'])){
						$this->saveScore($data['userid'], $score);
						$this->shout("Selamat ".$data['username']." jawaban anda benar (y) [b]".$this->getScore($data['userid'])."[/b] total skor anda.");
						echo "Selamat ".$data['username']." jawaban anda benar (y) ".$this->getScore($data['userid'])." total skor anda.\n";
						sleep(15);
						$score = 10;
						$soal = $lsoal[array_rand($lsoal)];
						echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
						$this->shout("Soal: ".$soal['pertanyaan']);
						$counter = time();
					}
				}
			}
			$lastid = $shout['lastid'];
			if((time() - $counter) == 20){ // 20 detik
				$score = 7;
				$this->shout("Waktu tinggal 20 detik.");
				echo "Waktu tinggal 20 detik.\n";
				sleep(1);
			}elseif((time() - $counter) == 60){ // 60 detik
				$this->shout("Waktu habis.");
				echo "Waktu habis.\n";
				$soal = $lsoal[array_rand($lsoal)];
				sleep(15);
				$score = 10;
				echo "\n---------------------\nSoal: ".$soal['pertanyaan']."\nJawaban: ".$soal['jawaban']."\n---------------------\n";
				$this->shout('Soal: '.$soal['pertanyaan']);
				$counter = time();
			}
		}
	}
}
$bot = new Trivia();

$bot->situs = "http://127.0.0.1/mybb/";
$bot->bot_userid = 2; //userid bot
$login = $bot->login('USERNAME', 'PASSWORD');
if($login){
	$bot->bot();
}else{
	echo "Bot gagal login -_-\n";
}