<?php
class bot_mybb{
    var $user_agent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)";
    var $cookies = "./cookie.txt";
    var $image_string = "EiAjj";
	var $image_hash = "4343dad3e5cc26c1a1772dc01034fc7b";
	var $situs;
	var $command;
	var $pemilik;
	var $username;
	var $password;
	var $qa;
	
	var $mysql_host;
	var $mysql_user;
	var $mysql_pass;
	var $mysql_dbname;
	
	function bot_simi(){
		$view = $this->mysb_view();
		if(($view['username'] != $this->username) && ($view['username'] != "")){
			$chat = $this->simsimi($view['chat']);
			if($this->mysb_shout($chat)){
				print $view['username']." = ".$view['chat']." >> ".$this->username." = ".$chat."\n";
			}
			if($view['username'] == $this->pemilik){
				$command = $this->mysb_command();
				if(eregi("logout",$command)){
					$this->mysb_shout("/me logout");
					$logout = $this->logout($situs);
					if($logout){
						print "Logout Berhasil.\n";
						exit;
					}else{
						print "Logout Gagal.\n";
					}
				}
			}
		}
		$this->bot_simi();
    }
    public function curl($url,$data="") {
        $ch = curl_init($url);
        if ($data != "") {
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookies); 
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookies);
        $source = curl_exec($ch);
        $this->status = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        $this->error = curl_error($ch);
        curl_close($ch);
        return $source;
    }
    public function login(){
        $data = array(
            "username" => $this->username,
            "password" => $this->password,
            "action" => "do_login",
			"imagestring"=>$this->image_string,
			"imagehash"=>$this->image_hash
        );
        $exec = $this->curl($this->situs."/member.php?action=login",$data);
        if(eregi("You have successfully been logged in.",$exec)){
			//$this->mysb_shout("/me bot ganteng hadir kembali :D");
            return true;
        }else{
            return false;
        }
    }
    public function logout($index=""){
        //preg_match("/(.*)logoutkey=([a-zA-Z0-9]+)\"/",$this->curl($this->situs."/index.php",""),$logout_key);
		if($index == ""){
			$key = $this->stringBetween('logoutkey=','"',$this->curl($this->situs."/index.php",""));
		}else{
			$key = $this->stringBetween('logoutkey=','"',$index);
		}
        $exec = $this->curl($this->situs."/member.php?action=logout&logoutkey=".$key,"");
        if(eregi("You have successfully been logged out.",$exec)){
            return true;
        }
        else{
            return false;
        }
    }
    public function getkey($index=""){
		if($index == ""){
			preg_match("/var my_post_key = \"(.*)\"/",$this->curl($this->situs."/index.php",""),$key);
		}else{
			preg_match("/var my_post_key = \"(.*)\"/",$index,$key);
		}
        return $key[1];
    }
    public function reputasi($komen,$uid){
        $url_rep = $this->situs."/reputation.php";
        $data_rep = array(
            "my_post_key"=>$this->getkey(),
            "action"=>"do_add",
            "uid"=>$uid,
            "pid"=>"0",
            "reputation"=>"1",
            "comments"=>$komen
        );
        $exec = $this->curl($url_rep,$data_rep);
        return $exec;
    }
	public function stringBetween($start, $end, $var){
		return preg_match('{'.preg_quote($start).'(.*?)'.preg_quote($end).'}s',$var,$m) ? $m[1] : '';
	}
    /* Myshoutbox */
    public function mysb_shout($chat){
        $data_chat = array("shout_data"=>$chat);
        $exec = $this->curl($this->situs."/xmlhttp.php?action=add_shout",$data_chat);
        return $exec;
    }
    public function mysb_viewShoutID($view=""){
		if($view==""){
			$view_sb = $this->curl($this->situs."/index.php?action=full_shoutbox","");
		}else{
			$view_sb = $view;
		}
		preg_match("/<tr id='shout-([0-9]+)'>/",$view_sb,$shout_id);
        return $shout_id[1];
    }
	public function mysb_validShoutID($shoutid,$view=""){
		if($view==""){
			$view_sb = $this->curl($this->situs."/index.php?action=full_shoutbox","");
		}else{
			$view_sb = $view;
		}
		if(preg_match("/<tr id='shout-".$shoutid."'>/",$view_sb)){
			return true;
		}else{
			return false;
		}
	}
    public function mysb_viewByShoutID($shoutid,$view=""){
		if($view==""){
			$cek = $this->mysb_validShoutID($shoutid);
		}else{
			$cek = $this->mysb_validShoutID($shoutid,$view);
		}
		if($cek){
			if($view==""){
				$view_sb = $this->curl($this->situs."/index.php?action=full_shoutbox","");
			}else{
				$view_sb = $view;
			}
			$sb = $this->stringBetween("<tr id='shout-".$shoutid."'>","</tr>",$view_sb);
			$userid = $this->stringBetween('<a href="./member.php?action=profile&uid=','"',$sb);
			preg_match("/<a href=\"\.\/member\.php\?action=profile&uid=([0-9]+)(.*?)>(.*?)<\/a>/",$sb,$username);
			$tanggal = $this->stringBetween('</a> - ',' --',$sb);
			$chat = $this->stringBetween('- '.$tanggal.' --','</span>',$sb);
			$hasil['shoutid'] = trim($shoutid);
			$hasil['uid'] = trim($userid);
			$hasil['username'] = strtolower(strip_tags(str_replace(" ","",trim($username[3]))));
			$hasil['tanggal'] = trim($tanggal);
			$hasil['chat'] = trim(strip_tags($chat));
			return $hasil;
		}
    }
    public function mysb_view($view=""){
		if($view==""){
			$view_sb = $this->curl($this->situs."/index.php?action=full_shoutbox","");
		}else{
			$view_sb = $view;
		}
		preg_match("/<tr id='shout-([0-9]+)'>/",$view_sb,$shout_id);
		$sb = $this->stringBetween("<tr id='shout-".$shout_id[1]."'>","</tr>",$view_sb);
        $userid = $this->stringBetween('<a href="./member.php?action=profile&uid=','"',$sb);
		preg_match("/<a href=\"\.\/member\.php\?action=profile&uid=([0-9]+)(.*?)>(.*?)<\/a>/",$sb,$username);
        $tanggal = $this->stringBetween('</a> - ',' --',$sb);
        $chat = $this->stringBetween('- '.$tanggal.' --','</span>',$sb);
		$hasil['shoutid'] = trim($shout_id[1]);
		$hasil['uid'] = trim($userid);
		$hasil['username'] = strtolower(strip_tags(trim($username[3])));
		$hasil['tanggal'] = trim($tanggal);
		$hasil['chat'] = trim(strip_tags($chat));
        return $hasil;
    }
	public function mysb_command(){
		$view_sb = $this->mysb_view();
		preg_match("/".$this->command."(.*)/",$view_sb['chat'],$comm);
		return $comm[1];
	}
    /* End Myshoutbox */
	
	/* bot simsimi */
	public function simsimi(){
		$shout_id = $this->mysb_viewShoutID();
		while(1){
			$load_sb = $this->curl($this->situs."/index.php?action=full_shoutbox","");
			$c = $this->mysb_viewByShoutID($shout_id,$load_sb);
			$lastChat = $c['shoutid'];
			$viewLastChat = file_get_contents('lastsimi.txt');
			preg_match("/".$this->command."(.*)/",$c['chat'],$comm);
			$command_spasi = explode(" ",trim($comm[1]));
			if($c){
				if($c['username'] == $this->pemilik){
					if(eregi("logout",$comm[1])){
						$this->mysb_shout("/me logout");
						$logout = $this->logout();
						if($logout){
							print "Logout Berhasil.\n";
							exit;
						}else{
							print "Logout Gagal.\n";
						}
					}elseif(trim($command_spasi[0]) == "simi" && trim($command_spasi[1]) == "stop"){
						$this->mysb_shout("/me bot simi dimatikan :D");
						$this->lastshoutid = $shout_id;
						break;
					}elseif(trim($command_spasi[0]) == "status"){
						$this->mysb_shout("/me sedang menjalankan bot simi.");
					}
				}
				if($c['username'] != strtolower($this->username)){
				$url = "http://www.simsimi.com/func/req?msg=".urlencode($c['chat'])."&lc=id";
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_REFERER, 'http://www.simsimi.com/talk.htm');
				curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookies); 
				curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookies);
				$source = curl_exec($ch);
				curl_close($ch);
				$json = json_decode($source);
				$jawab = trim(preg_replace('/\s+/',' ',str_replace("simi",$this->username,$json->response)));
				$this->mysb_shout($jawab);
				echo $jawab."\n";
				//echo $source;
				}
			if(($viewLastChat != $lastChat) && ($this->mysb_validShoutID($lastChat))){
				$shout_id = $shout_id+1;
				$catat = fopen('lastsimi.txt','w');
				fwrite($catat,$lastChat);
				
			}
			}
		}
	}
	
	/* start bot trivia */
	public function host($url){
		return parse_url($url, PHP_URL_HOST);
	}
	public function show_score($username){
		mysql_connect($this->mysql_host,$this->mysql_user,$this->mysql_pass);
		mysql_select_db($this->mysql_dbname);
		$query = mysql_query("SELECT * FROM `score` WHERE `situs`='".$this->host($this->situs)."' ORDER BY `total_skor` DESC");
		$no = 1;
		while($row = mysql_fetch_array($query)){
			$data[$row['username']] = "[b]".$row['username']."[/b] ada berada di rangking [b]".$no."[/b] dengan score [b]".$row['total_skor']."[/b].";
			$no++;
		}
		$cek = $data[$username];
		if($cek){
			$this->mysb_shout($cek);
			$status = $cek."\n";
		}else{
			$this->mysb_shout("Username [b]".$username."[/b] tidak ditemukan di database.");
			$status = "Username ".$username." tidak ditemukan di database.\n";
		}
		print $status;
	}
	public function show_highscore(){
		mysql_connect($this->mysql_host,$this->mysql_user,$this->mysql_pass);
		mysql_select_db($this->mysql_dbname);
		$query = mysql_query("SELECT * FROM `score` WHERE `situs`='".$this->host($this->situs)."' ORDER BY `total_skor` DESC LIMIT 0,5");
		$no = 1;
		$data .= "[b]-[high score]-[/b]\n";
		while($row = mysql_fetch_array($query)){
			$data .= $no.". [b]".$row['username']."[/b] dengan score [b]".$row['total_skor']."[/b].\n";
			$no++;
		}
		$this->mysb_shout($data);
		print $data;
	}
	public function trivia(){
		mysql_connect($this->mysql_host,$this->mysql_user,$this->mysql_pass);
		mysql_select_db($this->mysql_dbname);
		$query = mysql_query("SELECT * FROM `trivia`");
		while($row = mysql_fetch_array($query)){
			$asd[$row['pertanyaan']] = $row['jawaban'];
		}
		$this->qa = $asd;
		$acak = $this->qa[array_rand($this->qa)];
		$question = array_keys($this->qa,$acak);
		$answer = $this->qa[$question[0]];
		$this->mysb_shout($question[0]);
		print $question[0]." = ".$answer."\n";
		//$waktu = 1;
		$shout_id = $this->mysb_viewShoutID();
		$w = time();
		while(1){
			$load_sb = $this->curl($this->situs."/index.php?action=full_shoutbox","");
			$c = $this->mysb_viewByShoutID($shout_id,$load_sb);
			$lastChat = $c['shoutid'];
			$viewLastChat = file_get_contents('lastchat.txt');
			if($c){
			echo $lastChat."=>".$shout_id."=>".$c['username']."=>".$c['chat']."\n";
			preg_match("/".$this->command."(.*)/",$c['chat'],$comm);
			$command_spasi = explode(" ",trim($comm[1]));
			if($c['username'] == ""){
				$username = "userid".$c['uid'];
			}else{
				$username = $c['username'];
			}
			if($c['username'] == $this->pemilik){
				if(eregi("logout",$comm[1])){
					$this->mysb_shout("/me logout");
					$logout = $this->logout();
					if($logout){
						print "Logout Berhasil.\n";
						exit;
					}else{
						print "Logout Gagal.\n";
					}
				}elseif(trim($command_spasi[0]) == "trivia" && trim($command_spasi[1]) == "stop"){
					$this->mysb_shout("/me bot trivia dimatikan :D");
					$this->lastshoutid = $shout_id;
					break;
				}elseif(trim($command_spasi[0]) == "status"){
					$this->mysb_shout("/me sedang menjalankan bot trivia.");
				}
			}
			if(trim($comm[1]) == "highscore"){
				$this->show_highscore();
			}elseif(trim($command_spasi[0]) == "score"){
				if(trim($command_spasi[1]) == ""){
					$this->show_score(trim($username));
				}else{
					$this->show_score(trim($command_spasi[1]));
				}
			}
			if($c['chat'] == $answer){
				$get_user = mysql_query("SELECT * FROM `score` WHERE `username`='".$username."' AND `situs`='".$this->host($this->situs)."'");
				if(mysql_num_rows($get_user) == 0){
					mysql_query("INSERT INTO `score`(`id`,`username`,`total_skor`,`situs`) VALUES ('','".$username."','10','".$this->host($this->situs)."')");
				}else{
					mysql_query("UPDATE `score` SET total_skor=total_skor + 10 WHERE `username`='".$username."'");
				}
				$cek_skor = mysql_fetch_array(mysql_query("SELECT * FROM `score` WHERE `username`='".$username."' AND `situs`='".$this->host($this->situs)."'"));
				$this->mysb_shout($username." jawaban anda benar total skor anda ".$cek_skor['total_skor'].".");
				print $c['username']." jawaban anda benar.\n";
				$acak = $this->qa[array_rand($this->qa)];
				$question = array_keys($this->qa,$acak);
				$answer = $this->qa[$question[0]];
				sleep(4);
				$this->mysb_shout($question[0]);
				print $question[0]." = ".$answer."\n";
				$waktu = 1;
				$w = time();
			}
			if(($viewLastChat != $lastChat) && ($this->mysb_validShoutID($lastChat))){
				$shout_id = $shout_id+1;
				$catat = fopen('lastchat.txt','w');
				fwrite($catat,$lastChat);
				
			}
			}
			if((time()-$w) == 20){
				$this->mysb_shout("waktu tinggal 20 detik.");
				print "waktu tinggal 20 detik.\n";
				sleep(1);
			}elseif((time()-$w) == 40){
				$waktu = 0;
				$this->mysb_shout("waktu habis.");
				print "waktu habis.\n";
				$acak = $this->qa[array_rand($this->qa)];
				$question = array_keys($this->qa,$acak);
				$answer = $this->qa[$question[0]];
				sleep(4);
				$this->mysb_shout($question[0]);
				print $question[0]." = ".$answer."\n";
				$w = time();
			}
			//echo time()-$w."\n";
			//$waktu++;
		//sleep(1);
		}
	}
	/* end bot trivia */
	
	public function bot(){
		$shout_id = $this->mysb_viewShoutID();
		while(1){
			$load_sb = $this->curl($this->situs."/index.php?action=full_shoutbox","");
			$c = $this->mysb_viewByShoutID($shout_id,$load_sb);
			$lastChat = $c['shoutid'];
			$viewLastChat = file_get_contents('lastchat2.txt');
			if(($lastChat != $viewLastChat) && ($c['chat'] != "")){
				echo $lastChat."=>".$shout_id."=>".$c['username']."=>".$c['chat']."\n";
				preg_match("/".$this->command."(.*)/",$c['chat'],$comm);
				$command_spasi = explode(" ",trim($comm[1]));
				if($c['username'] == $this->pemilik){
					if(eregi("logout",$comm[1])){
						$this->mysb_shout("/me logout");
						$logout = $this->logout();
						if($logout){
							print "Logout Berhasil.\n";
							exit;
						}else{
							print "Logout Gagal.\n";
						}
					}elseif(trim($command_spasi[0]) == "trivia" && trim($command_spasi[1]) == "start"){
						$this->mysb_shout("/me bot trivia dijalankan :D");
						sleep(2);
						$this->trivia();
						$shout_id = $this->lastshoutid;
					}elseif(trim($command_spasi[0]) == "simi" && trim($command_spasi[1]) == "start"){
						$this->mysb_shout("/me bot simi dijalankan :D");
						sleep(2);
						$this->simsimi();
						$shout_id = $this->lastshoutid;
					}elseif(trim($command_spasi[0]) == "status"){
						$this->mysb_shout("/me sedang tidak melakukan apapun. :D");
					}
				}
				if(($viewLastChat != $lastChat) && ($this->mysb_validShoutID($lastChat))){
					$shout_id = $shout_id+1;
					$catat = fopen('lastchat2.txt','w');
					fwrite($catat,$lastChat);
					
				}
			}
		}
	}
}
$bot = new bot_mybb();

$bot->mysql_host = "localhost";
$bot->mysql_user = "root";
$bot->mysql_pass = "";
$bot->mysql_dbname = "trivia";

$bot->situs = "http://devilzc0de.org/forum/";
$bot->command = "!bot";
$bot->pemilik = "test";
$bot->username = "USERNAME";
$bot->password = "PASSWORD";
$bot->login();
$bot->bot();